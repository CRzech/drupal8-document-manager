<?php

/**
 * @file
 * Contains document_type.page.inc.
 *
 * Page callback for Document type entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Document type templates.
 *
 * Default template: document_type.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_document_type(array &$variables) {
  // Fetch DocumentType Entity Object.
  $document_type = $variables['elements']['#document_type'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
