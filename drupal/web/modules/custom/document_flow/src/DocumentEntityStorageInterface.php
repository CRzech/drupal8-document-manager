<?php

namespace Drupal\document_flow;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\document_flow\Entity\DocumentEntityInterface;

/**
 * Defines the storage handler class for Document entity entities.
 *
 * This extends the base storage class, adding required special handling for
 * Document entity entities.
 *
 * @ingroup document_flow
 */
interface DocumentEntityStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Document entity revision IDs for a specific Document entity.
   *
   * @param \Drupal\document_flow\Entity\DocumentEntityInterface $entity
   *   The Document entity entity.
   *
   * @return int[]
   *   Document entity revision IDs (in ascending order).
   */
  public function revisionIds(DocumentEntityInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Document entity author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Document entity revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\document_flow\Entity\DocumentEntityInterface $entity
   *   The Document entity entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(DocumentEntityInterface $entity);

  /**
   * Unsets the language for all Document entity with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
