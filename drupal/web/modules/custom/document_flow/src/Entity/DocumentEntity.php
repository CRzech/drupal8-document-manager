<?php

namespace Drupal\document_flow\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Document entity entity.
 *
 * @ingroup document_flow
 *
 * @ContentEntityType(
 *   id = "document_entity",
 *   label = @Translation("Document entity"),
 *   handlers = {
 *     "storage" = "Drupal\document_flow\DocumentEntityStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\document_flow\DocumentEntityListBuilder",
 *     "views_data" = "Drupal\document_flow\Entity\DocumentEntityViewsData",
 *     "translation" = "Drupal\document_flow\DocumentEntityTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\document_flow\Form\DocumentEntityForm",
 *       "add" = "Drupal\document_flow\Form\DocumentEntityForm",
 *       "edit" = "Drupal\document_flow\Form\DocumentEntityForm",
 *       "delete" = "Drupal\document_flow\Form\DocumentEntityDeleteForm",
 *     },
 *     "access" = "Drupal\document_flow\DocumentEntityAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\document_flow\DocumentEntityHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "document_entity",
 *   data_table = "document_entity_field_data",
 *   revision_table = "document_entity_revision",
 *   revision_data_table = "document_entity_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer document entity entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/document_entity/{document_entity}",
 *     "add-form" = "/admin/structure/document_entity/add",
 *     "edit-form" = "/admin/structure/document_entity/{document_entity}/edit",
 *     "delete-form" = "/admin/structure/document_entity/{document_entity}/delete",
 *     "version-history" = "/admin/structure/document_entity/{document_entity}/revisions",
 *     "revision" = "/admin/structure/document_entity/{document_entity}/revisions/{document_entity_revision}/view",
 *     "revision_revert" = "/admin/structure/document_entity/{document_entity}/revisions/{document_entity_revision}/revert",
 *     "revision_delete" = "/admin/structure/document_entity/{document_entity}/revisions/{document_entity_revision}/delete",
 *     "translation_revert" = "/admin/structure/document_entity/{document_entity}/revisions/{document_entity_revision}/revert/{langcode}",
 *     "collection" = "/admin/structure/document_entity",
 *   },
 *   field_ui_base_route = "document_entity.settings"
 * )
 */
class DocumentEntity extends RevisionableContentEntityBase implements DocumentEntityInterface
{

    use EntityChangedTrait;

    /**
     * {@inheritdoc}
     */
    public static function preCreate(EntityStorageInterface $storage_controller, array &$values)
    {
        parent::preCreate($storage_controller, $values);
        $values += [
            'user_id' => \Drupal::currentUser()->id(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function urlRouteParameters($rel)
    {
        $uri_route_parameters = parent::urlRouteParameters($rel);

        if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
            $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
        } elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
            $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
        }

        return $uri_route_parameters;
    }

    /**
     * {@inheritdoc}
     */
    public function preSave(EntityStorageInterface $storage)
    {
        parent::preSave($storage);

        foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
            $translation = $this->getTranslation($langcode);

            // If no owner has been set explicitly, make the anonymous user the owner.
            if (!$translation->getOwner()) {
                $translation->setOwnerId(0);
            }
        }

        // If no revision author has been set explicitly, make the document_entity owner the
        // revision author.
        if (!$this->getRevisionUser()) {
            $this->setRevisionUserId($this->getOwnerId());
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->get('name')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setName($name)
    {
        $this->set('name', $name);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getCreatedTime()
    {
        return $this->get('created')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setCreatedTime($timestamp)
    {
        $this->set('created', $timestamp);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getOwner()
    {
        return $this->get('user_id')->entity;
    }

    /**
     * {@inheritdoc}
     */
    public function getOwnerId()
    {
        return $this->get('user_id')->target_id;
    }

    /**
     * {@inheritdoc}
     */
    public function setOwnerId($uid)
    {
        $this->set('user_id', $uid);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setOwner(UserInterface $account)
    {
        $this->set('user_id', $account->id());
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function isPublished()
    {
        return (bool)$this->getEntityKey('status');
    }

    /**
     * {@inheritdoc}
     */
    public function setPublished($published)
    {
        $this->set('status', $published ? TRUE : FALSE);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public static function baseFieldDefinitions(EntityTypeInterface $entity_type)
    {
        $fields = parent::baseFieldDefinitions($entity_type);

        $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
            ->setLabel(t('Authored by'))
            ->setDescription(t('The user ID of author of the Document entity entity.'))
            ->setRevisionable(TRUE)
            ->setSetting('target_type', 'user')
            ->setSetting('handler', 'default')
            ->setTranslatable(TRUE)
            ->setDisplayOptions('view', [
                'label' => 'hidden',
                'type' => 'author',
                'weight' => 0,
            ])
            ->setDisplayOptions('form', [
                'type' => 'entity_reference_autocomplete',
                'weight' => 5,
                'settings' => [
                    'match_operator' => 'CONTAINS',
                    'size' => '60',
                    'autocomplete_type' => 'tags',
                    'placeholder' => '',
                ],
            ])
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);

        $fields['name'] = BaseFieldDefinition::create('string')
            ->setLabel(t('Titulo'))
            ->setDescription(t('Titulo del documento a enviar.'))
            ->setRevisionable(TRUE)
            ->setSettings([
                'max_length' => 50,
                'text_processing' => 0,
            ])
            ->setDefaultValue('')
            ->setDisplayOptions('view', [
                'label' => 'above',
                'type' => 'string',
                'weight' => -4,
            ])
            ->setDisplayOptions('form', [
                'type' => 'string_textfield',
                'weight' => -4,
            ])
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE)
            ->setRequired(TRUE);


        $fields['fid'] = BaseFieldDefinition::create('file')
            ->setLabel(t('Documento'))
            ->setDescription(t('El documento a enviar.'))
            ->setSetting('target_type', 'file')
            ->setSetting('file_extensions', 'jpg jpeg pdf')
            ->setSetting('file_directory', 'public://')
            ->setDisplayOptions('view', array(
                'type' => 'file', 'weight' => -8,
            ))
            ->setDisplayOptions('form', array(
                'type' => 'file', 'weight' => -8,
            ))
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);

        $fields['status'] = BaseFieldDefinition::create('boolean')
            ->setLabel(t('Publishing status'))
            ->setDescription(t('A boolean indicating whether the Document entity is published.'))
            ->setRevisionable(TRUE)
            ->setDefaultValue(FALSE)
            ->setDisplayOptions('form', [
                'type' => 'boolean_checkbox',
                'weight' => -3,
            ]);

        $fields['fecha_autorizacion'] = BaseFieldDefinition::create('datetime')
            ->setLabel(t('Fecha de la autorización'))
            ->setDescription(t('Fecha en la que el documento llego al estado de autorizado.'))
            ->setSetting('datetime_type', 'date')
            ->setRequired(true)
            ->setDisplayOptions('view', array(
                'label' => 'above',
                'type' => 'string',
                'weight' => -4,
            ))
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);

        $fields['fecha_revision'] = BaseFieldDefinition::create('datetime')
            ->setLabel(t('Fecha de la revisión'))
            ->setDescription(t('Fecha en la que el documento llego al estado de revisión.'))
            ->setSetting('datetime_type', 'date')
            ->setRequired(true)
            ->setDisplayOptions('view', array(
                'label' => 'above',
                'type' => 'string',
                'weight' => -4,
            ))
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);

        $fields['user_autorizacion'] = BaseFieldDefinition::create('entity_reference')
            ->setLabel(t('Autorizado por'))
            ->setDescription(t('El ID del usuario que autorizo este documento.'))
            ->setRevisionable(TRUE)
            ->setSetting('target_type', 'user')
            ->setSetting('handler', 'default')
            ->setTranslatable(TRUE)
            ->setDisplayOptions('view', [
                'label' => 'hidden',
                'type' => 'author',
                'weight' => 0,
            ])
            ->setDisplayOptions('form', [
                'type' => 'entity_reference_autocomplete',
                'weight' => 5,
                'settings' => [
                    'match_operator' => 'CONTAINS',
                    'size' => '60',
                    'autocomplete_type' => 'tags',
                    'placeholder' => '',
                ],
            ])
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);

        $fields['user_revision'] = BaseFieldDefinition::create('entity_reference')
            ->setLabel(t('Revisado por'))
            ->setDescription(t('El id del usuario que reviso este documento.'))
            ->setRevisionable(TRUE)
            ->setSetting('target_type', 'user')
            ->setSetting('handler', 'default')
            ->setTranslatable(TRUE)
            ->setDisplayOptions('view', [
                'label' => 'hidden',
                'type' => 'author',
                'weight' => 0,
            ])
            ->setDisplayOptions('form', [
                'type' => 'entity_reference_autocomplete',
                'weight' => 5,
                'settings' => [
                    'match_operator' => 'CONTAINS',
                    'size' => '60',
                    'autocomplete_type' => 'tags',
                    'placeholder' => '',
                ],
            ])
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);

        $fields['document_type_id'] = BaseFieldDefinition::create('entity_reference')
            ->setLabel(t('Tipo de documento'))
            ->setDescription(t('Que tipo de documento se esta subiendo al sistema.'))
            ->setRevisionable(TRUE)
            ->setSetting('target_type', 'document_type')
            ->setSetting('handler', 'default')
            ->setTranslatable(TRUE)
            ->setDisplayOptions('view', [
                'label' => 'hidden',
                'type' => 'author',
                'weight' => 0,
            ])
            ->setDisplayOptions('form', [
                'type' => 'entity_reference_autocomplete',
                'weight' => 5,
                'settings' => [
                    'match_operator' => 'CONTAINS',
                    'size' => '60',
                    'autocomplete_type' => 'tags',
                    'placeholder' => '',
                ],
            ])
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);

        $fields['created'] = BaseFieldDefinition::create('created')
            ->setLabel(t('Created'))
            ->setDescription(t('The time that the entity was created.'));

        $fields['changed'] = BaseFieldDefinition::create('changed')
            ->setLabel(t('Changed'))
            ->setDescription(t('The time that the entity was last edited.'));


        $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
            ->setLabel(t('Revision translation affected'))
            ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
            ->setReadOnly(TRUE)
            ->setRevisionable(TRUE)
            ->setTranslatable(TRUE);

        return $fields;
    }

}
