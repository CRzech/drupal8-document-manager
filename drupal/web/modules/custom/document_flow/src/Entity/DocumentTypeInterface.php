<?php

namespace Drupal\document_flow\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Document type entities.
 *
 * @ingroup document_flow
 */
interface DocumentTypeInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Document type name.
   *
   * @return string
   *   Name of the Document type.
   */
  public function getName();

  /**
   * Sets the Document type name.
   *
   * @param string $name
   *   The Document type name.
   *
   * @return \Drupal\document_flow\Entity\DocumentTypeInterface
   *   The called Document type entity.
   */
  public function setName($name);

  /**
   * Gets the Document type creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Document type.
   */
  public function getCreatedTime();

  /**
   * Sets the Document type creation timestamp.
   *
   * @param int $timestamp
   *   The Document type creation timestamp.
   *
   * @return \Drupal\document_flow\Entity\DocumentTypeInterface
   *   The called Document type entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Document type published status indicator.
   *
   * Unpublished Document type are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Document type is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Document type.
   *
   * @param bool $published
   *   TRUE to set this Document type to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\document_flow\Entity\DocumentTypeInterface
   *   The called Document type entity.
   */
  public function setPublished($published);

  /**
   * Gets the Document type revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Document type revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\document_flow\Entity\DocumentTypeInterface
   *   The called Document type entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Document type revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Document type revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\document_flow\Entity\DocumentTypeInterface
   *   The called Document type entity.
   */
  public function setRevisionUserId($uid);

}
