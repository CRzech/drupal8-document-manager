<?php

namespace Drupal\document_flow;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\document_flow\Entity\DocumentTypeInterface;

/**
 * Defines the storage handler class for Document type entities.
 *
 * This extends the base storage class, adding required special handling for
 * Document type entities.
 *
 * @ingroup document_flow
 */
class DocumentTypeStorage extends SqlContentEntityStorage implements DocumentTypeStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(DocumentTypeInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {document_type_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {document_type_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(DocumentTypeInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {document_type_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('document_type_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
