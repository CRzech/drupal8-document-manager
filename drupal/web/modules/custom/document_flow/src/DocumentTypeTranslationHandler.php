<?php

namespace Drupal\document_flow;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for document_type.
 */
class DocumentTypeTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
