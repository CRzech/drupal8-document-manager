<?php

namespace Drupal\document_flow\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class RevisarDocumento.
 */
class RevisarDocumento extends FormBase
{


    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'revisar_documento';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state, $document_id = NULL)
    {
        $form['document_id'] = [
            '#type' => 'value',
            '#value' => $document_id,
        ];
        $form['submit'] = [
            '#type' => 'submit',
            '#value' => $this->t('Marcar como revisado este documento'),
            '#attributes' => ['class' => ['btn', 'btn-success']]
        ];

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        parent::validateForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $user = \Drupal::currentUser();
        $values = $form_state->getValues();
        $document_entity = \Drupal::entityTypeManager()
            ->getStorage('document_entity')->load($values['document_id']);
        $messenger = \Drupal::messenger();
        $document_entity->set('user_revision', $user->id());
        $document_entity->set('fecha_revision', time());
        $document_entity->set('status', true);
        $document_entity->save();
        $messenger->addMessage($this->t('Documento revisado correctamente'), $messenger::TYPE_STATUS);

    }

}
