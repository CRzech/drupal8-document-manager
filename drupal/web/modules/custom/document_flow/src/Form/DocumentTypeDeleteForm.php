<?php

namespace Drupal\document_flow\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Document type entities.
 *
 * @ingroup document_flow
 */
class DocumentTypeDeleteForm extends ContentEntityDeleteForm {


}
