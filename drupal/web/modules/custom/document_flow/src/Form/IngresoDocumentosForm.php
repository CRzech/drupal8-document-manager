<?php

namespace Drupal\document_flow\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;

/**
 * Class IngresoDocumentosForm.
 */
class IngresoDocumentosForm extends FormBase
{


    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'ingreso_documentos_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {

        $entities = \Drupal::entityTypeManager()->getStorage("document_type")->loadMultiple();
        $options_types = [null => t('Seleccione un tipo de documento')];
        foreach ($entities as $entity) {
            $options_types[$entity->id()] = $entity->get('name')->getString();
        }
        $form['tipo_documento'] = [
            '#type' => 'select',
            '#options' => $options_types,
            '#required' => TRUE,
        ];
        $form['nombre'] = [
            '#type' => 'textfield',
            '#title' => t('Título del documento'),
            '#required' => TRUE,
        ];
        $form['documento'] = [
            '#type' => 'managed_file',
            '#title' => $this->t('Documento'),
            '#upload_location' => 'public://',
            '#upload_validators' => [
                'file_validate_extensions' => ['jpg jpeg pdf'],
            ],
            '#required' => TRUE,
        ];
        $form['submit'] = [
            '#type' => 'submit',
            '#value' => $this->t('Submit'),
        ];

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        parent::validateForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $user = \Drupal::currentUser();
        $values = $form_state->getValues();
        $form_file = $values['documento'];
        if (isset($form_file[0]) && !empty($form_file[0])) {
            try {
                $file = File::load($form_file[0]);
                $file->setPermanent();
                $file->save();
                $new_document = \Drupal::entityTypeManager()
                    ->getStorage('document_entity')
                    ->create([
                        'name' => $values['nombre'],
                        'user_id' => $user->id(),
                        'fid' => $file->id(),
                        'document_type_id' => $values['tipo_documento']
                    ])->save();
                $messenger = \Drupal::messenger();
                $messenger->addMessage($this->t('Documento enviado a autorización'), $messenger::TYPE_STATUS);
            }catch (\Exception $exception){
                ksm($exception);
                $messenger = \Drupal::messenger();
                $messenger->addMessage($this->t('Lo sentimos, tuvimos problemas para procesar el documento, por favor intentelo mas tarde'), $messenger::TYPE_ERROR);
            }
        }

    }

}
