<?php

namespace Drupal\document_flow\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Document entity entities.
 *
 * @ingroup document_flow
 */
class DocumentEntityDeleteForm extends ContentEntityDeleteForm {


}
