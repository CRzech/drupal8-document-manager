<?php

namespace Drupal\document_flow\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class EliminarDocumentosForm.
 */
class EliminarDocumentosForm extends FormBase
{


    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'eliminar_documentos_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state, $document_id = NULL)
    {
        $form['document_id'] = [
            '#type' => 'value',
            '#value' => $document_id,
        ];
        $form['submit'] = [
            '#type' => 'submit',
            '#value' => $this->t('Rechazar este documento'),
            '#attributes' => ['class' => ['btn', 'btn-danger']]
        ];

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        parent::validateForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $values = $form_state->getValues();
        $document_entity = \Drupal::entityTypeManager()
            ->getStorage('document_entity')->load($values['document_id']);
        $messenger = \Drupal::messenger();
        $document_entity->delete();
        $messenger->addMessage($this->t('Documento rechazado correctamente'), $messenger::TYPE_STATUS);
    }

}
