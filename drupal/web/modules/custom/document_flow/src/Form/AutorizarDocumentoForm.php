<?php

namespace Drupal\document_flow\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class AutorizarDocumentoForm.
 */
class AutorizarDocumentoForm extends FormBase
{


    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'autorizar_documento_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state, $document_id = NULL)
    {
        $form['document_id'] = [
            '#type' => 'value',
            '#value' => $document_id,
        ];
        $form['submit'] = [
            '#type' => 'submit',
            '#value' => $this->t('Autorizar documento'),
            '#attributes' => ['class' => ['btn', 'btn-primary']]
        ];
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        parent::validateForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $user = \Drupal::currentUser();
        $values = $form_state->getValues();
        $document_entity = \Drupal::entityTypeManager()
            ->getStorage('document_entity')->load($values['document_id']);
        $messenger = \Drupal::messenger();

        $document_entity->set('user_autorizacion', $user->id());
        $document_entity->set('fecha_autorizacion', time());
        $document_entity->save();
        $messenger->addMessage($this->t('Documento autorizado correctamente'), $messenger::TYPE_STATUS);
    }

}
