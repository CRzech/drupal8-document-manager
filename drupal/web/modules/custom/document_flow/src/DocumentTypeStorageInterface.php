<?php

namespace Drupal\document_flow;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\document_flow\Entity\DocumentTypeInterface;

/**
 * Defines the storage handler class for Document type entities.
 *
 * This extends the base storage class, adding required special handling for
 * Document type entities.
 *
 * @ingroup document_flow
 */
interface DocumentTypeStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Document type revision IDs for a specific Document type.
   *
   * @param \Drupal\document_flow\Entity\DocumentTypeInterface $entity
   *   The Document type entity.
   *
   * @return int[]
   *   Document type revision IDs (in ascending order).
   */
  public function revisionIds(DocumentTypeInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Document type author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Document type revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\document_flow\Entity\DocumentTypeInterface $entity
   *   The Document type entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(DocumentTypeInterface $entity);

  /**
   * Unsets the language for all Document type with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
