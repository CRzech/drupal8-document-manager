<?php

namespace Drupal\document_flow\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\file\Entity\File;

/**
 * Class DashboardDocumentosController.
 */
class DashboardDocumentosController extends ControllerBase
{

    /**
     * Dashboard.
     *
     * @return array Return Hello string.
     *   Return Hello string.
     */
    public function dashboard()
    {
        return [
            '#type' => 'markup',
            '#markup' => ''
        ];
    }

    public function documentDetails($id)
    {
        $data = [];
        $current_user = \Drupal::currentUser();
        $roles = $current_user->getRoles();
        $document_entity = \Drupal::entityTypeManager()
            ->getStorage('document_entity')->load($id);
        $document_type_entity = \Drupal::entityTypeManager()
            ->getStorage('document_type')->load($document_entity->get('document_type_id')->getString());
        $fid = $document_entity->get('fid')->getValue();
        $file = File::load(reset($fid)['target_id']);
        $file_uri = $file->getFileUri();
        $roles_autorizacion = array_column($document_type_entity->get('rids_autorizacion')->getValue(), 'target_id');
        $roles_revision = array_column($document_type_entity->get('rids_revision')->getValue(), 'target_id');
        $account_enviado = \Drupal\user\Entity\User::load($document_entity->get('user_id')->getString());
        $account_autorizado = \Drupal\user\Entity\User::load($document_entity->get('user_autorizacion')->getString());
        $account_revisado = \Drupal\user\Entity\User::load($document_entity->get('user_revision')->getString());
        $data['titulo_documento'] = $document_entity->get('name')->getString();
        $data['tipo_documento'] = $document_type_entity->get('name')->getString();
        $data['fecha_autorizacion'] = $document_entity->get('fecha_autorizacion')->getString();
        $data['url_documento'] = Url::fromUri(file_create_url($file_uri))->toString();
        $data['fecha_creado'] = date('d/m/Y H:i:s', $document_entity->get('created')->getString());
        $data['fecha_autorizado'] = strlen($document_entity->get('fecha_autorizacion')->getString()) >= 1 ? date('d/m/Y H:i:s', intval($document_entity->get('fecha_autorizacion')->getString())) : '';
        $data['fecha_revisado'] = strlen($document_entity->get('fecha_revision')->getString()) >= 1 ? date('d/m/Y H:i:s', $document_entity->get('fecha_revision')->getString()) : '';
        $data['usuario_enviado'] = $account_enviado->getUsername().' '.$account_enviado->getEmail();
        $data['usuario_autorizado'] = $account_autorizado != NULL ? $account_autorizado->getUsername().' '.$account_autorizado->getEmail() : '';
        $data['usuario_revisado'] = $account_revisado != NULL ? $account_revisado->getUsername().' '.$account_revisado->getEmail() : '';
        $form_autorizacion = NULL;
        $form_revision = NULL;
        $form_eliminar = NULL;
        if(strlen($data['fecha_revisado']) < 1){
            $form_eliminar = \Drupal::formBuilder()->getForm('\Drupal\document_flow\Form\EliminarDocumentosForm', $id);
        }
        if(count(array_intersect($roles,$roles_autorizacion)) >= 1 && strlen($data['fecha_autorizacion']) < 1){
            $form_autorizacion = \Drupal::formBuilder()->getForm('\Drupal\document_flow\Form\AutorizarDocumentoForm', $id);
        }
        if(count(array_intersect($roles,$roles_revision)) >= 1 && strlen($data['fecha_autorizacion']) >= 1 && strlen($data['fecha_revisado']) < 1){
            $form_revision = \Drupal::formBuilder()->getForm('\Drupal\document_flow\Form\RevisarDocumento', $id);
        }
        return [
            '#theme' => 'document_details',
            '#cache' => [
                'max-age' => 0
            ],
            '#data' => $data,
            '#form_eliminar' => $form_eliminar,
            '#form_autorizacion' => $form_autorizacion,
            '#form_revision' => $form_revision
        ];
    }


}
