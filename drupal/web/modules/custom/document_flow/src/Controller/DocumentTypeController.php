<?php

namespace Drupal\document_flow\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\document_flow\Entity\DocumentTypeInterface;

/**
 * Class DocumentTypeController.
 *
 *  Returns responses for Document type routes.
 */
class DocumentTypeController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a Document type  revision.
   *
   * @param int $document_type_revision
   *   The Document type  revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($document_type_revision) {
    $document_type = $this->entityManager()->getStorage('document_type')->loadRevision($document_type_revision);
    $view_builder = $this->entityManager()->getViewBuilder('document_type');

    return $view_builder->view($document_type);
  }

  /**
   * Page title callback for a Document type  revision.
   *
   * @param int $document_type_revision
   *   The Document type  revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($document_type_revision) {
    $document_type = $this->entityManager()->getStorage('document_type')->loadRevision($document_type_revision);
    return $this->t('Revision of %title from %date', ['%title' => $document_type->label(), '%date' => format_date($document_type->getRevisionCreationTime())]);
  }

  /**
   * Generates an overview table of older revisions of a Document type .
   *
   * @param \Drupal\document_flow\Entity\DocumentTypeInterface $document_type
   *   A Document type  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(DocumentTypeInterface $document_type) {
    $account = $this->currentUser();
    $langcode = $document_type->language()->getId();
    $langname = $document_type->language()->getName();
    $languages = $document_type->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $document_type_storage = $this->entityManager()->getStorage('document_type');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $document_type->label()]) : $this->t('Revisions for %title', ['%title' => $document_type->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all document type revisions") || $account->hasPermission('administer document type entities')));
    $delete_permission = (($account->hasPermission("delete all document type revisions") || $account->hasPermission('administer document type entities')));

    $rows = [];

    $vids = $document_type_storage->revisionIds($document_type);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\document_flow\DocumentTypeInterface $revision */
      $revision = $document_type_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $document_type->getRevisionId()) {
          $link = $this->l($date, new Url('entity.document_type.revision', ['document_type' => $document_type->id(), 'document_type_revision' => $vid]));
        }
        else {
          $link = $document_type->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.document_type.translation_revert', ['document_type' => $document_type->id(), 'document_type_revision' => $vid, 'langcode' => $langcode]) :
              Url::fromRoute('entity.document_type.revision_revert', ['document_type' => $document_type->id(), 'document_type_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.document_type.revision_delete', ['document_type' => $document_type->id(), 'document_type_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['document_type_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
