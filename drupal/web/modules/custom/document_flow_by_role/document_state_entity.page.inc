<?php

/**
 * @file
 * Contains document_state_entity.page.inc.
 *
 * Page callback for Document State entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Document State templates.
 *
 * Default template: document_state_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_document_state_entity(array &$variables) {
  // Fetch DocumentStateEntity Entity Object.
  $document_state_entity = $variables['elements']['#document_state_entity'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
