<?php

namespace Drupal\document_flow_by_role;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for document_state_entity.
 */
class DocumentStateEntityTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
