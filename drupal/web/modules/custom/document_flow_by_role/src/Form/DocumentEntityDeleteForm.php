<?php

namespace Drupal\document_flow_by_role\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Document entity entities.
 *
 * @ingroup document_flow_by_role
 */
class DocumentEntityDeleteForm extends ContentEntityDeleteForm {


}
