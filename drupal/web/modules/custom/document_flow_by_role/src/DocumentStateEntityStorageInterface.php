<?php

namespace Drupal\document_flow_by_role;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\document_flow_by_role\Entity\DocumentStateEntityInterface;

/**
 * Defines the storage handler class for Document State entities.
 *
 * This extends the base storage class, adding required special handling for
 * Document State entities.
 *
 * @ingroup document_flow_by_role
 */
interface DocumentStateEntityStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Document State revision IDs for a specific Document State.
   *
   * @param \Drupal\document_flow_by_role\Entity\DocumentStateEntityInterface $entity
   *   The Document State entity.
   *
   * @return int[]
   *   Document State revision IDs (in ascending order).
   */
  public function revisionIds(DocumentStateEntityInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Document State author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Document State revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\document_flow_by_role\Entity\DocumentStateEntityInterface $entity
   *   The Document State entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(DocumentStateEntityInterface $entity);

  /**
   * Unsets the language for all Document State with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
