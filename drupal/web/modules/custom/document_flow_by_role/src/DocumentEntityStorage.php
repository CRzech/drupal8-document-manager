<?php

namespace Drupal\document_flow_by_role;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\document_flow_by_role\Entity\DocumentEntityInterface;

/**
 * Defines the storage handler class for Document entity entities.
 *
 * This extends the base storage class, adding required special handling for
 * Document entity entities.
 *
 * @ingroup document_flow_by_role
 */
class DocumentEntityStorage extends SqlContentEntityStorage implements DocumentEntityStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(DocumentEntityInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {document_entity_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {document_entity_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(DocumentEntityInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {document_entity_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('document_entity_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
