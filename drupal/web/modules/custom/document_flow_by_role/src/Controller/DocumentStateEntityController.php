<?php

namespace Drupal\document_flow_by_role\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\document_flow_by_role\Entity\DocumentStateEntityInterface;

/**
 * Class DocumentStateEntityController.
 *
 *  Returns responses for Document State routes.
 */
class DocumentStateEntityController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a Document State  revision.
   *
   * @param int $document_state_entity_revision
   *   The Document State  revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($document_state_entity_revision) {
    $document_state_entity = $this->entityManager()->getStorage('document_state_entity')->loadRevision($document_state_entity_revision);
    $view_builder = $this->entityManager()->getViewBuilder('document_state_entity');

    return $view_builder->view($document_state_entity);
  }

  /**
   * Page title callback for a Document State  revision.
   *
   * @param int $document_state_entity_revision
   *   The Document State  revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($document_state_entity_revision) {
    $document_state_entity = $this->entityManager()->getStorage('document_state_entity')->loadRevision($document_state_entity_revision);
    return $this->t('Revision of %title from %date', ['%title' => $document_state_entity->label(), '%date' => format_date($document_state_entity->getRevisionCreationTime())]);
  }

  /**
   * Generates an overview table of older revisions of a Document State .
   *
   * @param \Drupal\document_flow_by_role\Entity\DocumentStateEntityInterface $document_state_entity
   *   A Document State  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(DocumentStateEntityInterface $document_state_entity) {
    $account = $this->currentUser();
    $langcode = $document_state_entity->language()->getId();
    $langname = $document_state_entity->language()->getName();
    $languages = $document_state_entity->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $document_state_entity_storage = $this->entityManager()->getStorage('document_state_entity');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $document_state_entity->label()]) : $this->t('Revisions for %title', ['%title' => $document_state_entity->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all document state revisions") || $account->hasPermission('administer document state entities')));
    $delete_permission = (($account->hasPermission("delete all document state revisions") || $account->hasPermission('administer document state entities')));

    $rows = [];

    $vids = $document_state_entity_storage->revisionIds($document_state_entity);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\document_flow_by_role\DocumentStateEntityInterface $revision */
      $revision = $document_state_entity_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $document_state_entity->getRevisionId()) {
          $link = $this->l($date, new Url('entity.document_state_entity.revision', ['document_state_entity' => $document_state_entity->id(), 'document_state_entity_revision' => $vid]));
        }
        else {
          $link = $document_state_entity->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.document_state_entity.translation_revert', ['document_state_entity' => $document_state_entity->id(), 'document_state_entity_revision' => $vid, 'langcode' => $langcode]) :
              Url::fromRoute('entity.document_state_entity.revision_revert', ['document_state_entity' => $document_state_entity->id(), 'document_state_entity_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.document_state_entity.revision_delete', ['document_state_entity' => $document_state_entity->id(), 'document_state_entity_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['document_state_entity_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
