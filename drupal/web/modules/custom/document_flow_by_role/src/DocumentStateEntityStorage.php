<?php

namespace Drupal\document_flow_by_role;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\document_flow_by_role\Entity\DocumentStateEntityInterface;

/**
 * Defines the storage handler class for Document State entities.
 *
 * This extends the base storage class, adding required special handling for
 * Document State entities.
 *
 * @ingroup document_flow_by_role
 */
class DocumentStateEntityStorage extends SqlContentEntityStorage implements DocumentStateEntityStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(DocumentStateEntityInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {document_state_entity_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {document_state_entity_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(DocumentStateEntityInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {document_state_entity_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('document_state_entity_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
