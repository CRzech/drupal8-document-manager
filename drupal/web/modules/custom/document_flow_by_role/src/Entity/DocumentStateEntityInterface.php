<?php

namespace Drupal\document_flow_by_role\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Document State entities.
 *
 * @ingroup document_flow_by_role
 */
interface DocumentStateEntityInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Document State name.
   *
   * @return string
   *   Name of the Document State.
   */
  public function getName();

  /**
   * Sets the Document State name.
   *
   * @param string $name
   *   The Document State name.
   *
   * @return \Drupal\document_flow_by_role\Entity\DocumentStateEntityInterface
   *   The called Document State entity.
   */
  public function setName($name);

  /**
   * Gets the Document State creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Document State.
   */
  public function getCreatedTime();

  /**
   * Sets the Document State creation timestamp.
   *
   * @param int $timestamp
   *   The Document State creation timestamp.
   *
   * @return \Drupal\document_flow_by_role\Entity\DocumentStateEntityInterface
   *   The called Document State entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Document State published status indicator.
   *
   * Unpublished Document State are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Document State is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Document State.
   *
   * @param bool $published
   *   TRUE to set this Document State to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\document_flow_by_role\Entity\DocumentStateEntityInterface
   *   The called Document State entity.
   */
  public function setPublished($published);

  /**
   * Gets the Document State revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Document State revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\document_flow_by_role\Entity\DocumentStateEntityInterface
   *   The called Document State entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Document State revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Document State revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\document_flow_by_role\Entity\DocumentStateEntityInterface
   *   The called Document State entity.
   */
  public function setRevisionUserId($uid);

}
