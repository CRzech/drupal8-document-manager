<?php

namespace Drupal\document_flow_by_role\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Document entity entity.
 *
 * @ingroup document_flow_by_role
 *
 * @ContentEntityType(
 *   id = "document_entity",
 *   label = @Translation("Document"),
 *   bundle_label = @Translation("Document type"),
 *   handlers = {
 *     "storage" = "Drupal\document_flow_by_role\DocumentEntityStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\document_flow_by_role\DocumentEntityListBuilder",
 *     "views_data" = "Drupal\document_flow_by_role\Entity\DocumentEntityViewsData",
 *     "translation" = "Drupal\document_flow_by_role\DocumentEntityTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\document_flow_by_role\Form\DocumentEntityForm",
 *       "add" = "Drupal\document_flow_by_role\Form\DocumentEntityForm",
 *       "edit" = "Drupal\document_flow_by_role\Form\DocumentEntityForm",
 *       "delete" = "Drupal\document_flow_by_role\Form\DocumentEntityDeleteForm",
 *     },
 *     "access" = "Drupal\document_flow_by_role\DocumentEntityAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\document_flow_by_role\DocumentEntityHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "document_entity",
 *   data_table = "document_entity_field_data",
 *   revision_table = "document_entity_revision",
 *   revision_data_table = "document_entity_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer document entity entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/document_entity/{document_entity}",
 *     "add-page" = "/admin/structure/document_entity/add",
 *     "add-form" = "/admin/structure/document_entity/add/{document_entity_type}",
 *     "edit-form" = "/admin/structure/document_entity/{document_entity}/edit",
 *     "delete-form" = "/admin/structure/document_entity/{document_entity}/delete",
 *     "version-history" = "/admin/structure/document_entity/{document_entity}/revisions",
 *     "revision" = "/admin/structure/document_entity/{document_entity}/revisions/{document_entity_revision}/view",
 *     "revision_revert" = "/admin/structure/document_entity/{document_entity}/revisions/{document_entity_revision}/revert",
 *     "revision_delete" = "/admin/structure/document_entity/{document_entity}/revisions/{document_entity_revision}/delete",
 *     "translation_revert" = "/admin/structure/document_entity/{document_entity}/revisions/{document_entity_revision}/revert/{langcode}",
 *     "collection" = "/admin/structure/document_entity",
 *   },
 *   bundle_entity_type = "document_entity_type",
 *   field_ui_base_route = "entity.document_entity_type.edit_form"
 * )
 */
class DocumentEntity extends RevisionableContentEntityBase implements DocumentEntityInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly, make the document_entity owner the
    // revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Document entity entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Document entity entity.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the Document entity is published.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    $fields['document'] = BaseFieldDefinition::create('file')
      ->setLabel('Document')
      ->setSettings([
        'uri_scheme' => 'public',
        'file_directory' => 'credentialing_providers',
        'file_extensions' => 'png jpg jpeg doc docx pdf xls xlsx',
      ])
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'file',
        'weight' => 0,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'file',
        'weight' => 0,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['state_document'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Document state'))
      ->setDescription(t('Current state of this document.'))
      ->setSetting('target_type', 'document_state_entity')
      ->setSetting('handler', 'default')
      ->setTargetEntityTypeId('document_state_entity')
      ->setDisplayOptions('view', array(
        'label'  => 'hidden',
        'type'   => 'project',
        'weight' => 0,
      ))
      ->setDisplayOptions('form', array(
        'type'     => 'select',
        'weight'   => 0,
        'settings' => array(
          'match_operator'    => 'CONTAINS',
          'size'              => '60',
          'autocomplete_type' => 'tags',
          'placeholder'       => '',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);



    return $fields;
  }

}
