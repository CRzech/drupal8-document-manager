<?php

namespace Drupal\document_flow_by_role\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Document entity type entities.
 */
interface DocumentEntityTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
