<?php

namespace Drupal\document_flow_by_role\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Document State entities.
 */
class DocumentStateEntityViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.

    return $data;
  }

}
