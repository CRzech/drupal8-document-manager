<?php

namespace Drupal\document_flow_by_role\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Document entity type entity.
 *
 * @ConfigEntityType(
 *   id = "document_entity_type",
 *   label = @Translation("Document entity type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\document_flow_by_role\DocumentEntityTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\document_flow_by_role\Form\DocumentEntityTypeForm",
 *       "edit" = "Drupal\document_flow_by_role\Form\DocumentEntityTypeForm",
 *       "delete" = "Drupal\document_flow_by_role\Form\DocumentEntityTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\document_flow_by_role\DocumentEntityTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "document_entity_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "document_entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/document_entity_type/{document_entity_type}",
 *     "add-form" = "/admin/structure/document_entity_type/add",
 *     "edit-form" = "/admin/structure/document_entity_type/{document_entity_type}/edit",
 *     "delete-form" = "/admin/structure/document_entity_type/{document_entity_type}/delete",
 *     "collection" = "/admin/structure/document_entity_type"
 *   }
 * )
 */
class DocumentEntityType extends ConfigEntityBundleBase implements DocumentEntityTypeInterface {

  /**
   * The Document entity type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Document entity type label.
   *
   * @var string
   */
  protected $label;

}
