<?php

namespace Drupal\document_flow_by_role;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Document State entity.
 *
 * @see \Drupal\document_flow_by_role\Entity\DocumentStateEntity.
 */
class DocumentStateEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\document_flow_by_role\Entity\DocumentStateEntityInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished document state entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published document state entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit document state entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete document state entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add document state entities');
  }

}
