<?php

namespace Drupal\document_flow_by_role;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Document State entities.
 *
 * @ingroup document_flow_by_role
 */
class DocumentStateEntityListBuilder extends EntityListBuilder {


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Document State ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\document_flow_by_role\Entity\DocumentStateEntity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.document_state_entity.edit_form',
      ['document_state_entity' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
