# Drupal 8 based document manager
This is a project template for drupal 8 that uses docksal and drupal composer project.
## References
- https://docs.docksal.io
- https://github.com/drupal-composer/drupal-project
## Installation
## Install docksal
**This is a one time setup - skip this if you already have a working Docksal environment.** 
Follow [Docksal environment setup instructions](http://docksal.readthedocs.io/en/master/getting-started/env-setup)
## Project setup
Just clone and run `fin init` and project should bring up
## Mailhog
This project comes with a mailhog implementation to catch e-mails sent by the site
```
    http://webmail.VIRTUALHOST.docksal
```
## Use docksal cli
You can install composer dependencies, run drush, run drupal console using the built-in docksal cli:
```
    cd drupal/
    fin drush cr
    fin composer require drupal/devel
```