@servers(['web' => 'deployer@96.30.195.228'])

@setup
    $repository = 'git@gitlab.com:CRzech/drupal8-document-manager.git';
    $app_dir = '/var/www/universidad/drupal8-document-manager';
    $branch = 'master';
@endsetup

@story('deploy')
    update_repository
@endstory


@task('update_repository', ['on' => 'web'])
    echo 'Updating repository'
    cd {{ $app_dir }}; git pull origin {{ $branch }}
@endtask