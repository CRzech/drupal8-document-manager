FROM tonisormisson/dev-lemp
# Installing usefull tools
RUN apt-get update
RUN apt-get install -y unzip wget zip

# Getting and installing composer
RUN wget https://getcomposer.org/composer.phar && chmod +x composer.phar && mv composer.phar /usr/local/bin/composer


# Installing Envoy via composer
RUN composer global require "laravel/envoy=~1.0"